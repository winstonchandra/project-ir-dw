import string
from joblib import load

def main():
	inputan = input("masukkan string: ")
	stringBenar = inputan.lower().split(" ")
	temp = []
	file = open("stopwords.txt")
	data = file.read().splitlines()
	file.close()
	hasilInputSudahDistem = []
	hasilInputDiClean = []
	for i in stringBenar:
		i = i.strip(string.punctuation)
		kata = stemmer(i)
		hasilInputDiClean.append(kata)
		if kata not in data:
			hasilInputSudahDistem.append(kata)
	pos_tag =  retrieve_post_tag(" ".join(hasilInputDiClean))
	result = processInput(" ".join(hasilInputSudahDistem) + " %s" % (pos_tag))
	print("Ada peluang %d persen berita tersebut adalah hoax" % (int(result * 100)))

# Method BELUM KELAR MSH ON PROGRESS
def processInput(inputBersih):
	vectorizer = load("tfidf.joblib")
	bayes = load("bayes.joblib")
	vector = vectorizer.transform([inputBersih])
	prediction = bayes.predict_proba(vector)
	return prediction[0][1]

def retrieve_post_tag(inputBersih):
	url = 'http://fws.cs.ui.ac.id/SampleClientPOSTag/SampleCLient'
	data = {'submit':'Get POS', 'inputWord' : inputBersih}
	try:
		r = requests.post(url, data = data, timeout= 3)
		soup = BeautifulSoup(r.text, 'html.parser')
		all_pos = soup.find_all('postag')
		pos_list = [J.text for J in all_pos]
		enamurated_pos = []
		for I in range(len(pos_list)):
			enumerated_pos.append("%d%s" % (I, pos_list[I]))
		return " ".join(enumerated_pos)
	except:
		return ""

# Blok stemmer
def stemmer(kata):
	temp = stemmerPre(kata)
	temp1 = stemmerPre(temp)
	temp2 = stemmerPre(temp1)
	temp3 = stemmerSuf(temp2)
	temp4 = stemmerSuf(temp3)
	return temp4

def stemmerPre(kata):
	if (len(kata) > 5):
		arrImbuhan2 = ["di","ke","se"]
		arrImbuhanSpec = ["me","pe"]
		vocal = ["a","i","u","e","o"]
		prefix = kata[:2]
		if(prefix in arrImbuhan2):
			if(kata[:5] == "diper"):
				return kata[5:]
			elif(kata[:3] == "kem"):
				return kata
			else:
				return kata[2:]
		elif(kata[:6] == "memper"):
			return kata[6:]
		elif(kata[:2] in arrImbuhanSpec and kata[2:4] == "ng" and kata[4] in vocal):
			return "k"+kata[4:]
		elif(kata[:2] in arrImbuhanSpec and kata[2:4] == "ny" and kata[4] in vocal):
			return "s"+kata[4:]
		elif(kata[:2] == "be" and kata[2] == "r" and kata[3] == "e" and kata[4] == "t"):
			return kata[3:]
		elif(kata[:2] == "be" and kata[2] == "r" and kata[3] == "e"):
			return kata[2:]
		elif(kata[:2] == "be" and kata[2] == "r" and kata[3] != "e"):
			return kata[3:]
		else:
			if(prefix in arrImbuhanSpec):
				char = kata[2]
				if(char == "m"):
					if(kata[:5] == "memak"):
						return kata[2:]
					else:
						return "p"+kata[3:]
				elif(char == "n"):
					return "t"+kata[3:]
	return kata

def stemmerSuf(kata):
	if(len(kata) > 5):
		arrImbuhanBlkg = ["kan","lah","nya","kah"]
		suffix = kata[-3:]
		if(suffix in arrImbuhanBlkg):
			return kata[:-3]
		elif(kata[-2:] == "an"):
			return kata[:-2]
		elif(kata[-1] == "i"):
			return kata[:-1]
	return kata