import pandas as pd
import stemmerIR
from random import shuffle
from sklearn.svm import LinearSVC
from sklearn.naive_bayes import MultinomialNB
df = pd.read_csv("compiled.csv", encoding = "ISO-8859-1")
df = df[['headline', 'label']]
stripped_headline = [''.join([K.lower() for K in I if K.isalpha() or K == " "]).strip() for I in df['headline'].values]
df['headline'] = stripped_headline
tokenList = []
wordSet = set()
for lines in stripped_headline:
    tokens = [K for K in lines.split(" ") if K != ""]
    for token in tokens:
        wordSet.add(token)
    tokenList.append(tokens)
dictionaryHasilStemmer = {}
for I in wordSet:
    a = stemmerIR.stemmerPre(I)
    a = stemmerIR.stemmerPre(a)
    a = stemmerIR.stemmerPre(a)
    a = stemmerIR.stemmerSuf(a)
    a = stemmerIR.stemmerSuf(a)
    dictionaryHasilStemmer[I] = a
tokenListSudahDiStem = []
for row in tokenList:
    hasil = []
    for item in row:
        hasil.append(dictionaryHasilStemmer[item])
    tokenListSudahDiStem.append(" ".join(hasil))
from sklearn.feature_extraction.text import CountVectorizer
vector = CountVectorizer(min_df  = 2)
X = vector.fit_transform(tokenListSudahDiStem)
y = df['label'] == 'asli'
y = y.values
indeksDataYangAsli = []
indeksDataYangPalsu = []
for I in range(len(y)):
    if y[I] == True:
        indeksDataYangAsli.append(I)
    else:
        indeksDataYangPalsu.append(I)
shuffle(indeksDataYangAsli)
shuffle(indeksDataYangPalsu)
indeksDataAsli_training = indeksDataYangAsli[:1463]
indeksDataAsli_test = indeksDataYangAsli[1463:1626]
indeksDataPalsu_training = indeksDataYangPalsu[:1463]
indeksDataPalsu_test = indeksDataYangPalsu[1463:1626]
indeks_training = indeksDataAsli_training + indeksDataPalsu_training
indeks_test = indeksDataAsli_test + indeksDataPalsu_test
X_train = X[indeks_training]
y_train = y[indeks_training]
X_test  = X[indeks_test]
y_test  = y[indeks_test]
model = MultinomialNB()
model.fit(X_train,y_train)
prediction = model.predict(X_test)
benar = 0
for I in range(len(prediction)):
    if prediction[I] == y_test[I]:
        benar += 1
print(benar / len(y_test))
model_2 = LinearSVC(random_state=0, tol=1e-5)
model_2.fit(X_train,y_train)
prediction_2 = model.predict(X_test)
benar = 0
for I in range(len(prediction_2)):
    if prediction_2[I] == y_test[I]:
        benar += 1
print(benar / len(y_test))