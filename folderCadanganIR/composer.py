import pandas as pd

file_winston = open("dataSet2.csv")
file_deo = open("hasil.txt")

dataFrame_dictionary = {'headline' : [], 'label' : []}

teks = file_winston.readlines()
file_winston.close()
asli = list(set(teks))
print(len(asli))

for I in asli:
    dataFrame_dictionary['headline'].append(I.replace(",nh",""))
    dataFrame_dictionary['label'].append('asli')
teks = file_deo.readlines()
for row in teks:
    label , headline = row.split(" -- ")
    if label == "true":
        dataFrame_dictionary['headline'].append(headline)
        dataFrame_dictionary['label'].append('asli')
    else:
        dataFrame_dictionary['headline'].append(headline)
        dataFrame_dictionary['label'].append('hoax')
file_deo.close()
df = pd.DataFrame(dataFrame_dictionary)
df.to_csv("compiled.csv")