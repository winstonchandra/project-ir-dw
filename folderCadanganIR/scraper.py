from bs4 import BeautifulSoup
import requests
import re
contoh_salah = ["hoax", "fitnah", "hasut", "misinformasi", "disinformasi", 'framing', 'salah', 'false']

for X in range(1,119):
    r = requests.get('https://turnbackhoax.id/page/%d/' % (X))
    berita = r.text
    soup = BeautifulSoup(berita, 'html.parser')
    judul_judul = soup.find_all('a',{'class':"mh-excerpt-more"})
    for i in judul_judul:
        hasil = i['title'].encode("utf-8").decode('ascii', errors='ignore').replace(" : "," ").split(" ")
        label = hasil[0]
        teks  = " ".join(hasil[1:])
        for I in contoh_salah:
            if I.lower() in label.lower():
                print("false" + " -- " + teks)
                break
        else:
            print("true" + " -- " + teks)
