from bs4 import BeautifulSoup
import requests
import re
import openpyxl
count = 1
# Menyiapkan module excel untuk dituliskan data-data scrapping
wb = openpyxl.load_workbook(filename='dataSet.xlsx')
arrBerita = []
# Loop melakukan scrapping berita not hoax politik dari website metro tv dan menuliskan data ke file excel
for cnt in range(1, 115):
	r = requests.get('http://pemilu.metrotvnews.com/scroll/'+str(cnt)+'/')
	berita = r.text
	soup = BeautifulSoup(berita, 'html.parser')
	judul_judul = soup.find_all('h2')
	for i in judul_judul:
		res = re.sub('<i>|</i>','',i.a['title'])
		if res not in arrBerita:
			arrBerita.append(res)
			ws = wb.worksheets[0]
			ws['A'+str(count)] = res
			ws['B'+str(count)] = "nh"
			count = count + 1
# Loop melakukan scrapping berita not hoax kultur dari website metro tv dan menuliskan data ke file excel
for cnt in range(1, 32):
	r = requests.get('http://hiburan.metrotvnews.com/scroll/'+str(cnt)+'/')
	berita = r.text
	soup = BeautifulSoup(berita, 'html.parser')
	judul_judul = soup.find_all('h2')
	for i in judul_judul:
		res = re.sub('<i>|</i>','',i.a['title'])
		if res not in arrBerita:
			arrBerita.append(res)
			ws = wb.worksheets[0]
			ws['A'+str(count)] = res
			ws['B'+str(count)] = "nh"
			count = count + 1
wb.save('dataSet.xlsx')
