Kelompok DWIR
Nama Anggota:
- Kerenza Doxolodeo - 1606881670
- Winston Chandra - 1606831294

Ini adalah proyek IR mengenai Text Classification yang berfungsi sebagai Deteksi berita hoax dan kultur. Model yang digunakan 
untuk produksi digenerate di folderCadanganIR/model.ipynb. Evaluasi 10-fold Cross Validation dapat dilihat di 
folderCadanganIR/neural_network-sklearn.ipynb. Kedua file harus dibuka dengan Jupyter notebook

File-file yang dibutuhkan untuk scraiping juga ada di folderCadanganIR (seperti parser.py, composer.py, datasetIRBeritaNonHoax.py)

Cara penggunaan aplikasi versi developer:

Untuk mengrun website (dengan menggunakan localhost), silakan memanggil
```python manage.py runserver 8000```

Modul-modul yang dibutuhkan untuk mengrun website dapat diperiksa di requirments.txt