from django.apps import AppConfig


class HoaxdetectorConfig(AppConfig):
    name = 'hoaxdetector'
