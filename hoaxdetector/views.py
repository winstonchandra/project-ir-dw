from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json
import string
from joblib import load

# Create your views here.
# Ini adalah method untuk merender halaman utama
def index(request):
	response = {}
	html = 'hoaxdetect/hoaxdetect.html'
	return render(request, html, response)

# Method untuk menerima input dari pengguna dan mengolah input tersebut dengan metode naive bayes classifier, pos tagging, dibuang 
# stopwordsnya, stemming dan juga mengembalikan output dalam bentuk JSON untuk ditampilkan di front end
@csrf_exempt
def main(request):
	if request.method == 'POST':
		inputan = request.POST['paramm']
		stringBenar = inputan.lower().split(" ")
		temp = []
		file = open("hoaxdetector/stopwords.txt")
		data = file.read().splitlines()                      # Membuka file berisi kumpulan stopwords dan menyimpannya ke dalam array
		file.close()
		hasilInputSudahDistem = []
		hasilInputDiClean = []
		for i in stringBenar:
			i = i.strip(string.punctuation)                   # Membuang tanda baca pada inputan dari pengguna
			kata = stemmer(i)                                 # Melakukan metode stemming pada inputan dari user
			hasilInputDiClean.append(kata)
			if kata not in data:                             # Kondisi untuk menghilangkan stopwords
				hasilInputSudahDistem.append(kata)
		pos_tag =  retrieve_post_tag(" ".join(hasilInputDiClean))                   # Memanggilan fungsi pos tagging terhadap inputan dari pengguna
	                                                                                # yang sudah dibersihkan dari tanda baca, imbuhan, dan stopwords
		result = processInput(" ".join(hasilInputSudahDistem) + " %s" % (pos_tag))
		res = {"hasil":"Probabilitas berita ini hoax adalah %d persen"% (int(result * 100))}
		return JsonResponse(res)

# Ini adalah method untuk mencari nilai tdidf dan juga probabilitas hoax dari inputan yang diberikan pada parameter dengan memakai 
# metode Naive Bayes Classfier, bila ingin dilihat lebih rinci untuk metode kalkulasi probabilitasnya, dapat menjalankan file tfidf.joblib 
# dan bayes.joblib pada jupyter (Apabila ingin melihat kode untuk melakukan kalkulasi tersebut)
def processInput(inputBersih):
	vectorizer = load("hoaxdetector/tfidf.joblib")
	bayes = load("hoaxdetector/bayes.joblib")
	vector = vectorizer.transform([inputBersih])
	prediction = bayes.predict_proba(vector)
	return prediction[0][1]

# Method untuk melakukan pos tagging terhadap input yang diberikan dengan memakai web service dari Fasilkom UI
def retrieve_post_tag(inputBersih):
	url = 'http://fws.cs.ui.ac.id/SampleClientPOSTag/SampleCLient'
	data = {'submit':'Get POS', 'inputWord' : inputBersih}
	try:
		r = requests.post(url, data = data, timeout= 3)
		soup = BeautifulSoup(r.text, 'html.parser')
		all_pos = soup.find_all('postag')
		pos_list = [J.text for J in all_pos]
		enamurated_pos = []
		for I in range(len(pos_list)):
			enumerated_pos.append("%d%s" % (I, pos_list[I]))
		return " ".join(enumerated_pos)
	except:
		return ""

# Fungsi untuk melakukan stemmer
def stemmer(kata):
	temp = stemmerPre(kata)
	temp1 = stemmerPre(temp)
	temp2 = stemmerPre(temp1)
	temp3 = stemmerSuf(temp2)
	temp4 = stemmerSuf(temp3)
	return temp4

# Fungsi untuk melakukan stemming pada imbuhan di depan seperti (di-, ke-, se-, me-,mem-,pem-,peng-,peny-,meng-,meny-, kem-, diper-,memper-
# ,kem-,memak- (untuk kasus kata memakan))
def stemmerPre(kata):
	if (len(kata) > 5):
		arrImbuhan2 = ["di","ke","se"]
		arrImbuhanSpec = ["me","pe"]
		vocal = ["a","i","u","e","o"]
		prefix = kata[:2]
		if(prefix in arrImbuhan2):
			if(kata[:5] == "diper"):
				return kata[5:]
			elif(kata[:3] == "kem"):
				return kata
			else:
				return kata[2:]
		elif(kata[:6] == "memper"):
			return kata[6:]
		elif(kata[:2] in arrImbuhanSpec and kata[2:4] == "ng" and kata[4] in vocal):
			return "k"+kata[4:]
		elif(kata[:2] in arrImbuhanSpec and kata[2:4] == "ny" and kata[4] in vocal):
			return "s"+kata[4:]
		elif(kata[:2] == "be" and kata[2] == "r" and kata[3] == "e" and kata[4] == "t"):
			return kata[3:]
		elif(kata[:2] == "be" and kata[2] == "r" and kata[3] == "e"):
			return kata[2:]
		elif(kata[:2] == "be" and kata[2] == "r" and kata[3] != "e"):
			return kata[3:]
		else:
			if(prefix in arrImbuhanSpec):
				char = kata[2]
				if(char == "m"):
					if(kata[:5] == "memak"):
						return kata[2:]
					else:
						return "p"+kata[3:]
				elif(char == "n"):
					return "t"+kata[3:]
	return kata

# Method untuk melakukan stemming terhadap imbuhan belakang seperti (-kan, -lah, -nya, -kah, -an, -i)
def stemmerSuf(kata):
	if(len(kata) > 5):
		arrImbuhanBlkg = ["kan","lah","nya","kah"]
		suffix = kata[-3:]
		if(suffix in arrImbuhanBlkg):
			return kata[:-3]
		elif(kata[-2:] == "an"):
			return kata[:-2]
		elif(kata[-1] == "i"):
			return kata[:-1]
	return kata